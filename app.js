$(function() {

	let header = $('#header')
	let intro = $('#intro')

	let introH = intro.innerHeight()
	let scrollPosition = $(window).scrollTop() 

	let zero = 0
	let navToggle = $('#navToggle')
	let nav = $('#nav')
	checkScroll(scrollPosition, zero)

	

	/* Sticky header*/
	// $(window).on('scroll load resize', function() {
	// 	scrollPosition = $(this).scrollTop()

	// 	checkScroll(scrollPosition)
	// })

	// function checkScroll(scrollPosition) {
	// 	if (scrollPosition > introH) {
	// 		header.addClass('fixed')
	// 	} else {
	// 		header.removeClass('fixed')
	// 	}
	// }



	/* Hide/Show header */
	$(window).on("scroll resize", function() {
		scrollPosition = $(this).scrollTop()

		header.toggleClass("hide", scrollPosition > zero)
		zero = scrollPosition

		checkScroll(scrollPosition, zero)
	})

	function checkScroll(scrollPosition, zero) {
		zero = scrollPosition
		if (scrollPosition > 50) {
			header.addClass("fixed")
		} else {
			header.removeClass("fixed")
		}
	}



	/* Smooth scroll */
	$('[data-scroll').on('click', function() {
		event.preventDefault();

		let elementID = $(this).data('scroll');
		let elementOffset = $(elementID).offset().top

		nav.removeClass('show')		

		$('html, body').animate({
			scrollTop: elementOffset - 100
		}, 750)
	})



	/* Nav Toggle */
	navToggle.on('click', function(event) {
		event.preventDefault()
		
		if (nav.hasClass('show')) {

			nav.stop().animate({
				opacity: '0.1',
				top: '150%'
			}, 'easy', function() {
				$(this).removeAttr('style')
			})
		} else {
			nav.stop().animate({
				opacity: '1',
				top: '100%'
			}, 'easy')
		}
		nav.toggleClass('show')
	})


	/* Reviews */
	let slider = $('#reviewsSlider')
	slider.slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: false,
		dots: true
	});

	let small={width: "200px",height: "100px"};
    let large={width: "160px",height: "100px"};
    let count=1; 
    $("#imgtab").css(small).hover(function () { 
        $(this).animate((count==1)?large:small);
        count = 1-count;
    });
});